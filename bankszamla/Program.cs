﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bankszamla
{
    
    class Program
    {
                       
        static void Main(string[] args)
        {
            Bank bank = new Bank();
            bank.szamlaletrehoz(0,"",0);
            bank.szamlaletrehoz(1, "Nagy Sándor", 10000);
            bank.szamlaletrehoz(2, "Mária Terézia", 50000);
            bank.szamlaletrehoz(3, "Rákóczi Ferenc", 20000);
            bank.listaz();
            bank.atutal(2,1,5000);
            bank.atutal(2, 3, 5000);

            bank.szamlak[3].betesz(10000); // bank.betesz(3,10000)
            bank.szamlak[1].kivesz(1000);  // bank.kivesz(1,1000)
            bank.listaz();


            Console.WriteLine("\n10000 HUF feltöltése a következő számlaszámra: 1");
            Console.WriteLine("Egyenleg feltöltés előtt: " + bank.szamlak[bank.szamlak.FindIndex(x=>x.szamlaszam == 1)].lekerdez() + " HUF");
            bank.Transaction(ETransaction.Deposit, 1, 10000);
            Console.WriteLine("Egyenleg feltöltés után: " + bank.szamlak[bank.szamlak.FindIndex(x => x.szamlaszam == 1)].lekerdez() + " HUF");

            Console.WriteLine("\n1000 HUF kivétele a következő számlaszámról: 1");
            Console.WriteLine("Egyenleg kivétel előtt: " + bank.szamlak[bank.szamlak.FindIndex(x => x.szamlaszam == 1)].lekerdez() + " HUF");
            bank.Transaction(ETransaction.Withdraw, 1, 1000);
            Console.WriteLine("Egyenleg kivétel után: " + bank.szamlak[bank.szamlak.FindIndex(x => x.szamlaszam == 1)].lekerdez() + " HUF");

            #region commented
            //Szamla sz1 = new Szamla(1,50000);
            //Szamla sz2 = new Szamla(2, 1000000);
            //szamlak.Add(new Szamla(0,0));

            //szamlak.Add(new Szamla(1, 50000));
            //szamlak.Add(new Szamla(2, 1000000));
            //sz1.egyenleg = 100000;
            //Console.Write(sz1.egyenleg);
            //Console.WriteLine(szamlak[1].lekerdez());
            //Console.WriteLine(szamlak[2].lekerdez());
            //szamlak[1].betesz(20000);
            //szamlak[2].betesz(100000);
            //Console.WriteLine(szamlak[1].lekerdez());
            //Console.WriteLine(szamlak[2].lekerdez());
            //sz1.kivesz(10000);
            //sz2.kivesz(50000);
            //Console.WriteLine(sz1.lekerdez());
            //Console.WriteLine(sz2.lekerdez());

            //listaz();
            #endregion
            Console.ReadLine();
        }
    }
}
