﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bankszamla
{
    class Bank
    {
        public List<Szamla> szamlak = new List<Szamla>();

        public void szamlaletrehoz(int szamlaszam, string tulajdonos, int egyenleg)
        {
            szamlak.Add(new Szamla(szamlaszam, tulajdonos, egyenleg));
        }

        public void atutal(int honnan, int hova, int osszeg)
        {
            szamlak[honnan].kivesz(osszeg);
            szamlak[hova].betesz(osszeg);
        }

        public void listaz() //Feladat a 0. elem kihagyása listázás során
        {
            
            foreach (Szamla elem in szamlak)
            {
                if(elem.tulajdonos != "" && elem != null)
                    Console.WriteLine(elem.szamlaszam + " " +elem.tulajdonos + " " + elem.lekerdez());
            }
        }
        // betesz és kivesz method!
        
        public void Transaction(ETransaction type, int account, int amount)
        {
            switch (type)
            {
                case ETransaction.Withdraw:
                    {
                        
                        szamlak[account].kivesz(amount);
                        
                        break;
                    }
                case ETransaction.Deposit:
                    {
                        szamlak[account].betesz(amount);
                        break;
                    }
            }
            Log.Create("transaction.log", Encoding.UTF8, string.Format($"({Enum.GetName(typeof(ETransaction), (int)type)}) account: {account} amount: {amount}"));
        }

    }
}
